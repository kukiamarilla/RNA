/**
 * Estructura y métodos de Red Neuronal
 *
 * @author Kuki Amarilla
 *
 **/
#include <math.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdio.h>
struct Peso
{
	// Estructura para la lista de pesos de cada neurona
	
	double peso;
	double copia;
	struct Peso * sig;
};

struct Capa
{
	// Estructura para la lista de capas de la red
	int tamanho;
	struct Neurona * neuronas;
	struct Capa * sig;
	struct Capa * ant;
};

struct Neurona
{
	// Estructura para las neuronas de la red

	double bias;
	double salida;
	double * backPropagation;
	int calculado;
	struct Peso * pesos;
	struct Neurona * sig;
	struct Capa * capa;
};

long getMicrotime(){
	// Obtiene el tiempo en microsegundos del sistema
	struct timeval currentTime;
	gettimeofday(&currentTime, NULL);
	return currentTime.tv_sec * (int)1e6 + currentTime.tv_usec;
}

double sigmoide(double x)
{
	// Calcula la sigmoide de x
	return 1/ (1 + pow(M_E, -1 * x));

}

struct Capa * nuevaCapa()
{
	// Crea una capa y retorna su apuntador

	struct Capa * nueva = (struct Capa *) malloc(sizeof(struct Capa));
	nueva->tamanho = 0;
	nueva->sig = NULL;
	nueva->ant = NULL;
	nueva->neuronas = NULL;
	return nueva;
}

void agregarCapa(struct Capa * capa, struct Capa * red)
{
	// Agrega una capa a la red

	while(red->sig != NULL){
		red = red->sig;
	}
	red->sig = capa;
	capa->ant = red;
}

struct Neurona * nuevaNeurona()
{
	// Crea una neurona y retorna su apuntador

	struct Neurona * nueva = (struct Neurona *)malloc(sizeof(struct Neurona));
	nueva->bias = 0.5;
	nueva->sig = NULL;
	nueva->pesos = NULL;
	return nueva;
}

void agregarPeso(double peso, struct Neurona * neurona)
{
	// Agrega un peso sináptico a una neurona

	struct Peso * nuevo = (struct Peso *)malloc(sizeof(struct Peso));
	nuevo->peso = peso;
	nuevo->sig = NULL;
	struct Peso * aux;

	if(neurona->pesos == NULL){
		neurona->pesos = nuevo;
	}else{
		aux = neurona->pesos;
		while(aux->sig != NULL){
			aux = aux->sig;
		}
		aux->sig = nuevo;
	}
}

void agregarNeurona(struct Neurona * neurona, struct Capa * capa)
{	
	// Agrega la neurona especificada a la capa especificada

	struct Neurona * aux;
	struct Capa * capaAux;
	if( capa->neuronas == NULL ){
		capa->neuronas = neurona;
	}else{
		aux = capa->neuronas;
		while(aux->sig != NULL){
			aux = aux->sig;
		}
		aux->sig = neurona;
	}
	neurona->capa = capa;

	
	// Agrega los pesos sinápticos correspondientes a la capa siguiente

	if(capa->sig != NULL){
		capaAux = capa->sig;
		aux = capaAux->neuronas;
		while(aux != NULL){
			agregarPeso(0.5, aux);
			aux = aux->sig;
		}
	}


	// Agrega los pesos sinápticos a la nueva neurona por cada neurona de la capa anterior

	if(capa->ant != NULL){
		capaAux = capa->ant;
		aux = capaAux->neuronas;
		while(aux != NULL){
			agregarPeso(0.5, neurona);
			aux = aux->sig;
		}
	}

	
	// Se actualiza el tamaño de la capa

	capa->tamanho++;
}

void modificarBias(double bias, struct Neurona * neurona)
{
	// Modifica el bias de la neurona indicada

	neurona->bias = bias;
}

struct Peso * obtenerPeso(int index, struct Neurona * neurona)
{
	// Obtiene el peso especificado de una neurona
	
	struct Peso * peso = neurona->pesos;
	while(index > 0){
		if(peso == NULL){
			return NULL;
		}
		peso = peso->sig;
		index--;
	}
	return peso;
}

struct Neurona * obtenerNeurona(int index, struct Capa *  capa)
{
	// Obtiene la neurona especificada de una capa

	struct Neurona * neurona = capa->neuronas;
	while(index > 0){
		if(neurona == NULL){
			return NULL;
		}
		neurona = neurona->sig;
		index--;
	}
	return neurona;
}

int obtenerIndice(struct Neurona * neurona)
{
	// Obtiene el índice de una neurona

	struct Neurona * aux = neurona->capa->neuronas;
	int i = 0;
	while(aux != neurona){
		i++;
		aux = aux->sig;
	}
	return i;
}

struct Capa * obtenerSalidas(struct Capa * capa)
{
	//  Obtiene la capa de salida
	while(capa->sig != NULL){
		capa = capa->sig;
	}
	return capa;
}
void modificarPeso(double peso, int index, struct Neurona * neurona)
{
	// Modifica el peso indicado de una neurona

	struct Peso * aux = obtenerPeso(index, neurona);
	aux->peso = peso;
}

struct Capa * nuevaRed(int entradas)
{
	// Crea una nueva red neuronal con una capa de entrada

	struct Capa * red = nuevaCapa();
	struct Neurona * aux;
	for (int i = 0; i < entradas; ++i){
		aux = nuevaNeurona();
		modificarBias(0, aux);
		agregarPeso(1, aux);
		agregarNeurona(aux, red);
	}
	return red;
}

void inicializarRed(struct Capa * red)
{
	// Pone todas las neuronas como no calculadas

	if(red->sig != NULL)
		inicializarRed(red->sig);
	struct Neurona * aux = red->neuronas;
	while(aux != NULL){
		aux->calculado = 0;
		aux = aux->sig;
	}
}

double calcularSalida(double parametros [], struct Neurona * neurona)
{
	// Retorna la salida de la neurona indicada
	if(neurona->calculado == 1)
		return neurona->salida;
	int salida = 0;
	struct Peso * auxPeso = neurona->pesos;
	struct Neurona * auxNeurona = NULL;
	int c;
	if(neurona->capa->ant == NULL){
		auxNeurona = neurona->capa->neuronas;
		c=0;
		while(auxNeurona != neurona){
			auxNeurona = auxNeurona->sig;
			c++;
		}
		neurona->salida = parametros[c];
	}else{
		auxNeurona = neurona->capa->ant->neuronas;
		while(auxPeso != NULL){
			salida += auxPeso->peso * calcularSalida(parametros, auxNeurona);
			auxPeso = auxPeso->sig;
			auxNeurona = auxNeurona->sig;
		}
		neurona->salida = sigmoide(salida + neurona->bias);
	}

	return neurona->salida;

}

double * forwardPropagation(double parametros [], struct Capa * red)
{
	// Calcula la salida de la red

	int c = 0;
	
	inicializarRed(red);
	
	
	while(red->sig != NULL)
		red = red->sig;

	double * salidas = (double *)malloc(red->tamanho * sizeof(double));

	struct Neurona * neurona = red->neuronas;
	
	while(neurona != NULL){
		salidas[c] = calcularSalida(parametros, neurona);
		c++;
		neurona = neurona->sig;
	}
	
	return salidas;
}

void copiarPesos(struct Capa * red)
{
	// Hace una copia de todos los pesos de la red
	if(red->sig != NULL)
		copiarPesos(red->sig);
	struct Neurona * auxNeurona = red->neuronas;
	struct Peso * auxPeso;
	while(auxNeurona != NULL){
		auxPeso = auxNeurona->pesos;
		while(auxPeso != NULL){
			auxPeso->copia = auxPeso->peso;
			auxPeso = auxPeso->sig;
		}
		auxNeurona = auxNeurona->sig;
	}
}

double * corregirPesos(double resultados [], struct Neurona * neurona)
{

	// Funcion recursiva de aprendizaje
	struct Neurona * aux;
	int salidas = obtenerSalidas(neurona->capa)->tamanho;
	double * backAux;
	double * backPropagation = (double *) malloc (salidas * sizeof(double));
	double derivadaPeso;
	struct Peso * auxPeso;
	int index = obtenerIndice(neurona);

	if(neurona->calculado == 0){
		
		if(neurona->capa->sig != NULL){
			aux = neurona->capa->sig->neuronas;
			while(aux != NULL){
				backAux = corregirPesos(resultados, aux);
				for (int i = 0; i < salidas; ++i)
					backPropagation[i] += obtenerPeso(index, aux)->copia * backAux[i];
				aux = aux->sig;
			}
			for (int i = 0; i < salidas; ++i)
				backPropagation[i] *= neurona->salida * (1 - neurona->salida);
		}else{
			backPropagation[index] = neurona->salida * (1 - neurona->salida) * -1 * (resultados[index] - neurona->salida);
		}
		for (int i = 0; i < salidas; ++i){
			neurona->bias -= backPropagation[i];
		}
		aux = neurona->capa->ant->neuronas;
		auxPeso = neurona->pesos;
		while(aux != NULL){
			derivadaPeso = 0;
			for (int i = 0; i < salidas; ++i){
				derivadaPeso += backPropagation[i] * auxPeso->copia;
			}
			auxPeso->peso -= derivadaPeso;
			aux = aux->sig;
			auxPeso = auxPeso->sig;
		}
		
		neurona->backPropagation = backPropagation;
		neurona->calculado = 0;

		return backPropagation;
	}

	return neurona->backPropagation;
}

void backPropagation(double parametros [], double resultados [], struct Capa * red)
{
	// Ejecuta el algoritmo de aprendizaje
	copiarPesos(red);
	forwardPropagation(parametros, red);
	red = red->sig;
	struct Neurona * neurona = red->neuronas;
	while(neurona != NULL){
		corregirPesos(resultados, neurona);
		neurona = neurona->sig;
	}
}

void aleatorizarPesos(struct Capa * red)
{
	// Inicializar todos los pesos sinápticos como aleatorios

	if(red->sig != NULL)
		aleatorizarPesos(red->sig);
	struct Neurona * auxNeurona = red->neuronas;
	struct Peso * auxPeso;
	if(red->ant != NULL){
		while(auxNeurona != NULL){
			srand(getMicrotime()%10);
			auxNeurona->bias = (float)((float)(rand() % 1000) / 100 - 5);
			auxPeso = auxNeurona->pesos;
			while(auxPeso != NULL){
				srand(getMicrotime()%10);
				auxPeso->peso = (float)((float)(rand() % 1000) / 100 - 5);
				auxPeso = auxPeso->sig;
				usleep(10);
			}
			auxNeurona = auxNeurona->sig;
		}
	}
}

void imprimirRed(struct Capa * red)
{
	// Imprime la red neuronal

	int capaN, neuronaN;

	capaN = 0;

	while(red != NULL){
		struct Neurona * auxNeurona = red->neuronas;
		struct Peso * auxPeso;
		neuronaN = 0;

		printf("Capa %d\n", capaN);

		while(auxNeurona != NULL){

			printf("\tNeurona %d: Umbral %lf\n", neuronaN, auxNeurona->bias);

			auxPeso = auxNeurona->pesos;
			while(auxPeso != NULL){
				printf("\t\t%lf\n", auxPeso->peso);
				auxPeso = auxPeso->sig;
			}
			auxNeurona = auxNeurona->sig;
			neuronaN++;
		}
		red = red->sig;
		capaN++;
	}
}
